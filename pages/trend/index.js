import { Fragment, useEffect, useState } from 'react'

import moment from 'moment' 
import AppHelper from '../../app_helper'
import NavBar from '../../components/NavBar'
import BarChart from '../../components/BarChart'
import LineChart from '../../components/LineChart'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'


export default function trend(){

	const [token, setToken] = useState('')
	const [incomeRecords, setIncomeRecords] = useState([])
	const [expenseRecords, setExpenseRecords] = useState([])
	
	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[])

	useEffect(()=>{
		
		if(token){

			const options = {

				headers: {

						
					Authorization: `Bearer ${token}`

				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all-income`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setIncomeRecords(data)
				
			})	

			fetch(`${AppHelper.API_URL}/users/get-all-expenses`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setExpenseRecords(data)
				
			})

		}

	},[token])

	return (


			<Fragment>
				<NavBar />

				{
				token !==null
				?
				<Fragment>
					<Container className="mt-4">
					<LineChart incomeRecords={incomeRecords} expenseRecords={expenseRecords} />
					</Container>
				</Fragment>
				
				:
                <Fragment>
                <div className="errorPage">
                  <h5>403 Forbidden</h5>
                  
                  <Button variant="dark" href="/">Go to Login Page</Button>
                </div>
            
                </Fragment>

			}


				
				<footer className="footerBar fixed-bottom">
					© Copyright {AppHelper.dateToday.getFullYear()} 
				</footer>	
			</Fragment>


	)

}		

//{expenseRecords}
//<Line incomeRecords={incomeRecords} expenseRecords={expenseRecords} />